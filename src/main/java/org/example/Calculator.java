package org.example;

public class Calculator {
    /**
     * Accepts two integers and return addition of a and b
     *
     * @return a+B
     */
    public int plus() {
        return plus(0);
    }

    /**
     * Accepts two integers and return addition of a and b
     *
     * @param a
     * @return a+B
     */
    public int plus(int a) {
        return plus(a, 0);
    }

    /**
     * Accepts two integers and return addition of a and b
     * @param a
     * @param b
     * @return a+B
     */
    public int plus(int a,int b){
        return a+b;
    }
    /**
     * Accepts array of integers and return addition of array values
     * @param list
     * @return res
     */
    public int plus(int[] list){
        int res = 0;
        for (int num: list){
            res+=num;
        }
        return res;
    }
}
