package org.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CalculatorCornerTest {
    protected Calculator calculator = new Calculator();
    @Test
    public void testPlusWithZeroArguments() {
        int result = calculator.plus();
        assertEquals(0, result);
    }

    @Test
    public void testPlusWithOneArgument() {
        int result = calculator.plus(7);
        assertEquals(7, result);
    }
    @Test
    public void testPlusWithTenArguments() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int result = calculator.plus(numbers);
        assertEquals(55, result);
    }
    @Test
    public void testPlusWithMoreThanTenArguments() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        int result = calculator.plus(numbers);
        assertEquals(91, result);
    }
}
