package org.example;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculatorTest {
    protected Calculator calculator = new Calculator();
    int a;
    int b;
    int expected;

    public CalculatorTest(int a, int b,int expected) {
        this.a = a;
        this.b = b;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {1, -7, -6},
                {2, 5, 7},
                {2, -38, -36},
                {-5, 34, 29}
        });
    }
    @Test
    public void testPlus(){
        int result = calculator.plus(a,b);
        assertEquals(expected,result);
    }

}
